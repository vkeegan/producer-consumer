/*
 * Vivien Keegan | vivienkeegan@gmail.com
 * CSC 4320: Operating Systems | Summer 2016
 * Programming Assignment 2
 *
 * buffer.c 
 * Producer-Consumer Solution Using PThreads
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define TRUE 1
#define BUFFER_SIZE 5

typedef int buffer_item;

buffer_item number;
buffer_item buffer[BUFFER_SIZE];
int insertPointer = 0, removePointer = 0;
pthread_mutex_t mutex;
sem_t empty;
sem_t full;

int insert_item();
int remove_item(buffer_item *item);
void *producer(void *param);
void *consumer(void *param);

int insert_item() {
	if (!sem_wait(&empty) && !pthread_mutex_lock(&mutex)) {
		buffer[insertPointer] = number++;
		
		printf("Producer %u produced %d\n", pthread_self(), buffer[insertPointer]);

		if (!sem_post(&full)) {
			if (++insertPointer == BUFFER_SIZE) {
				insertPointer = 0;
			}

			return pthread_mutex_unlock(&mutex);
		}	
	}

	return -1;
}


int remove_item(buffer_item *item) {
	if (!sem_wait(&full) && !pthread_mutex_lock(&mutex)) {
		*item = buffer[removePointer];

		if (!sem_post(&empty)) {
			if (++removePointer == BUFFER_SIZE) {
				removePointer = 0;
			}
			return pthread_mutex_unlock(&mutex);
		}
	}

	return -1;
}


int main(int argc, char *argv[]) {
	int producerThreads, consumerThreads;
	int i;
	int sleepTime;

	/* Get command line arguments */
	if (argc != 5) {
		fprintf(stderr, "Useage: <sleep time> <producer threads> <consumer threads> <start number>\n");
		
		return -1;
	}

	sleepTime = atoi(argv[1]);
	producerThreads = atoi(argv[2]);
	consumerThreads = atoi(argv[3]);
	number = atoi(argv[4]);

	/* Initialize the synchronization tools */
	printf("%d\n", sem_init(&empty, 0, BUFFER_SIZE));
	printf("%d\n", sem_init(&full, 0, 0));
	printf("%d\n", pthread_mutex_init(&mutex, NULL));

	/* Create the producer and consumer threads */
	pthread_t tid;

	for (i = 0; i < producerThreads; i++) {
		pthread_create(&tid, NULL, (void *)producer, NULL);
	}

	for (i = 0; i < consumerThreads; i++) {
		pthread_create(&tid, NULL, (void *)consumer, NULL);
	}

	/* Sleep for user specified time */
	sleep(sleepTime);

	return 0;
}

void *producer(void *param) {
	while (TRUE) {
		sleep(rand() % BUFFER_SIZE);

		if (insert_item()) {
			printf("Producer %u: insert_item failed.\n", pthread_self());
		}	
	}
}

void *consumer(void *param) {
	while (TRUE) {
		buffer_item item;

		sleep(rand() % BUFFER_SIZE);

		if (remove_item(&item)) {
			printf("Consumer %u: remove_item failed.\n", pthread_self());
		} else {
			printf("Consumer %u consumed %d\n", pthread_self(), item);
		}
	}
}