###Producer-Consumer Problem
This is a solution to the Producer-Consumer Problem done as a homework assignment for CSC 4320 - Operating Systems (Summer 2016).

Program requirements are as specified in the textbook Operating System Concepts, 9th ed. by Silberschatz, et al. (page 253) with some modifications by the class professor.

Some initial code (function prototypes and code to read the command line input) was provided by the class instructor as a skeleton.